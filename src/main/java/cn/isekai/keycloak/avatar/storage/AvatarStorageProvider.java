package cn.isekai.keycloak.avatar.storage;

import org.keycloak.provider.Provider;

import java.io.InputStream;
import java.util.Map;

/**
 * 头像存储提供者
 */
public interface AvatarStorageProvider extends Provider {

    /**
     * 判断头像是否存在
     *
     * @param realmName   realm name
     * @param userId      user id
     * @param avatarId    avatar id
     * @param defaultSize default size
     * @return true if the avatar exists
     */
    boolean hasAvatar(String realmName, String userId, String avatarId, String defaultSize);

    /**
     * 保存头像
     *
     * @param realmName realm name
     * @param userId    user id
     * @param avatarId  avatar id
     * @param input     input stream
     * @param size      size
     * @return true if the avatar is saved
     */
    boolean saveAvatarImage(String realmName, String userId, String avatarId, InputStream input, String size);

    /**
     * 保存头像
     *
     * @param realmName  realm name
     * @param userId     user id
     * @param avatarId   avatar id
     * @param input      input stream
     * @param cropParams crop params
     * @param sizeList   size list
     * @return true if the avatar is saved
     */
    boolean saveOriginalAvatarImage(String realmName, String userId, String avatarId, InputStream input,
                                    AvatarCropParams cropParams, Map<String, Integer> sizeList);

    /**
     * 加载头像
     *
     * @param realmName       realm name
     * @param userId          user id
     * @param avatarId        avatar id
     * @param size            size
     * @param fallbackDefault fallback default
     * @return input stream
     */
    InputStream loadAvatarImage(String realmName, String userId, String avatarId, String size, boolean fallbackDefault);

    /**
     * 获取头像URL
     *
     * @param realmName       realm name
     * @param userId          user id
     * @param avatarId        avatar id
     * @param size            size
     * @param fallbackDefault fallback default
     * @return avatar url
     */
    String getAvatarURL(String realmName, String userId, String avatarId, String size, boolean fallbackDefault);

    /**
     * 获取头像URL模板
     *
     * @param realmName realm name
     * @param userId    user id
     * @param avatarId  avatar id
     * @return avatar url template
     */
    String getAvatarURLTemplate(String realmName, String userId, String avatarId);

    /**
     * 删除头像
     *
     * @param realmName realm name
     * @param userId    user id
     * @param avatarId  avatar id
     * @param sizeList  size list
     * @return true if the avatar is removed
     */
    boolean removeAvatar(String realmName, String userId, String avatarId, Iterable<String> sizeList);

    /**
     * 获取资源
     * @return resource
     */
    Object getResource();
}
