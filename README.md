# Keycloak头像插件

参考了[thomasdarimont](https://github.com/mai1015/keycloak-avatar-minio-extension)和[mai1015](https://github.com/mai1015/keycloak-avatar-extension)的代码。

运行环境要求：
- Keycloak 23.x.x
- Java 17

## 配置
### spi.avatar-provider.config.
| 键                | 类型            | 描述                                | 默认值         |
| ----------------- | --------------- | ----------------------------------- | -------------- |
| defaultAvatar     | ```string```    | 默认头像的URL                        | ```/```        |
| alwaysRedirect    | ```boolean```   | 是否始终重定向到静态图像URL          | ```false```    |
| defaultSize       | ```string```    | 默认头像大小                          | ```lg```       |
| storageService    | ```string```    | 存储头像文件的提供程序                | ```file```     |

### spi.avatar-storage.file.
| 键            | 类型         | 描述                                     | 默认值                                              |
| -------------- | ------------ | ---------------------------------------- | ---------------------------------------------------- |
| root           | ```string``` | 文件存储根路径                           | ```/```                                              |
| route          | ```string``` | 头像文件的URL和路径（无需更改）           | ```/{realm}/avatar/{avatar_id}/avatar-{size}.png```  |
| baseurl        | ```string``` | Keycloak基本URL                          | ```/realms/```                                       |
| default-avatar | ```string``` | 默认头像的路径                           | ```/{realm}/avatar/default.png```                    |

## API
### 基本参数
| 键    | 类型                                                      | 描述            | 默认值                              |
| ------ | --------------------------------------------------------- | --------------- | ----------------------------------- |
| size   | ```enum<String>("xs", "sm", "md", "lg", "xl", "xxl")```   | 头像文件大小    | ```{Config.defaultSize}```          |
| format | ```enum<String>("raw", "json")```                         | 响应格式        | ```raw```                          |

### 通过用户ID获取头像
请求:
```
GET /realms/{realm}/avatar/by-userid/{user_id}
```

响应 (当format=json时):
```js
{
    status: 1,
    avatar: "", // lg尺寸的头像url
    avatar_tpl: "", // 头像url模板，占位符：%s
}
```

### 通过用户名获取头像
请求:
```
GET /realms/{realm}/avatar/by-username/{username}
```

响应 (当format=json时):
```js
{
    status: 1,
    avatar: "", // lg尺寸的头像url
    avatar_tpl: "", // 头像url模板，占位符：%s
}
```

### 获取当前登录用户的头像
警告：此API仅应在Keycloak的前端使用。

请求:
```
GET /realms/{realm}/avatar/by-username/{username}
```

响应 (当format=json时):
```js
{
    status: 1,
    avatar: "", // lg尺寸的头像url
    avatar_tpl: "", // 头像url模板，占位符：%s
}
```

### 获取默认头像
请求:
```
GET /realms/{realm}/avatar/default
```

响应 (当format=json时):
```js
{
    status: 1,
    avatar: "", // lg尺寸的头像url
    avatar_tpl: "", // 头像url模板，占位符：%s
}
```

### 设置用户头像
请求:
```
POST {用于GET的任何端点}

image=<图像文件>
```